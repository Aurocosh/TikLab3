﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Priority_Queue;

namespace TIKLab
{
    public enum DecodingState { Decoded, ErrorIncorrectSymbol, ErrorCodeIsTooLong}

    public class HoffmanCodeBuilder
    {
        public TreeNode RootNode { get; private set; }
        private readonly Dictionary<BitArray, char> _decodingDictionary;
        private readonly Dictionary<char, BitArray> _encodingDictionary;

        public HoffmanCodeBuilder(int[] occurances, char[] alphabet)
        {
            CalculateCodeTree(occurances, alphabet);
            _decodingDictionary = new Dictionary<BitArray, char>();
            _encodingDictionary = new Dictionary<char, BitArray>();
            CollectNodeCodes(RootNode, new BitArray(0));
        }

        private void CalculateCodeTree(int[] occurances, char[] alphabet)
        {
            var nodeCount = occurances.Length + occurances.Length - 1;
            var queue = new FastPriorityQueue<TreeNode>(nodeCount);

            var nodes = occurances.Select((v,i) => new TreeNode(v, alphabet[i])).ToArray();
            foreach (var node in nodes)
                queue.Enqueue(node, node.SymbolCount);

            while (queue.Count>1)
            {
                var leftNode = queue.Dequeue();
                var rightNode = queue.Dequeue();
                var parentNode = new TreeNode(leftNode, rightNode);
                queue.Enqueue(parentNode, parentNode.SymbolCount);
            }
            RootNode = queue.Dequeue();
        }

        private void CollectNodeCodes(TreeNode node, BitArray currentCode)
        {
            if (node.IsLeaf)
            {
                _decodingDictionary[currentCode] = node.Symbol;
                _encodingDictionary[node.Symbol] = currentCode;
                return;
            }
            if (node.GotLeftNode)
            {
                var leftCode = (BitArray)currentCode.Clone();
                var codeLength = leftCode.Length;
                leftCode.Length = codeLength + 1;
                leftCode[codeLength] = false;
                CollectNodeCodes(node.LeftNode, leftCode);
            }
            if (node.GotRightNode)
            {
                var rightCode = (BitArray)currentCode.Clone();
                var codeLength = rightCode.Length;
                rightCode.Length = codeLength + 1;
                rightCode[codeLength] = true;
                CollectNodeCodes(node.RightNode, rightCode);
            }
        }

        public string GetCodes()
        {
            var codes = _encodingDictionary.Select(v => v.Key + " = " + v.Value.ToBitString()).ToList();
            return string.Join("\n", codes);
        }

        public string EncodeString(string input)
        {
            var stringBuilder = new StringBuilder();

            foreach (var symbol in input)
            {
                if (_encodingDictionary.ContainsKey(symbol))
                {
                    stringBuilder.Append(_encodingDictionary[symbol].ToBitString());
                }
            }

            return stringBuilder.ToString();
        }

        public string DecodeString(string input, out DecodingState state)
        {
            var builder = new StringBuilder();
            var code = new BitArray(0);
            var maxSymbolLength = _decodingDictionary.Select(v => v.Key.Length).Max();

            foreach (var bitChar in input)
            {
                var length = code.Length;
                code.Length = length + 1;

                if (bitChar == '1')
                    code[length] = true;
                else if (bitChar == '0')
                    code[length] = true;
                else
                {
                    state = DecodingState.ErrorIncorrectSymbol;
                    return builder.ToString();
                }

                if (_decodingDictionary.ContainsKey(code))
                {
                    builder.Append(_decodingDictionary[code]);
                    code = new BitArray(0);
                }
                else if (code.Length > maxSymbolLength)
                {
                    state = DecodingState.ErrorCodeIsTooLong;
                    return builder.ToString();
                }
            }
            state = DecodingState.Decoded;
            return builder.ToString();
        }
    }
}
