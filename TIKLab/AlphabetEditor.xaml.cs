﻿using System.IO;
using System.Text.RegularExpressions;
using System.Windows;
using Microsoft.Win32;

namespace TIKLab
{
    /// <summary>
    /// Interaction logic for AlphabetEditor.xaml
    /// </summary>
    public partial class AlphabetEditor : Window
    {
        private readonly string _text;
        public bool DeleteNonAlphabetSymbols { get; set; }
        public AlphabetEditor(string text, string alphabet)
        {
            InitializeComponent();
            _text = text;
            AlphabetEditBox.Text = alphabet;
            DeleteNonAlphabetCheck.DataContext = this;
        }

        private void LoadAlphabetButton_OnClick(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
                AlphabetEditBox.Text = File.ReadAllText(openFileDialog.FileName);
        }

        private void GenerateAlphabet_OnClick(object sender, RoutedEventArgs e)
        {
            var alphabet = TextStatAnalyzer.TextToAlphabet(_text);
            AlphabetEditBox.Text = new string(alphabet);
        }

        public string GetAlphabet()
        {
            var alphabet = AlphabetEditBox.Text.Trim();
            return alphabet==""?null : alphabet;
        }

        private void ClearUp_OnClick(object sender, RoutedEventArgs e)
        {
            Regex rgx = new Regex(@"[^a-zA-Zа-яА-Я\. ]");
            AlphabetEditBox.Text = rgx.Replace(AlphabetEditBox.Text, "");
        }
    }
}
