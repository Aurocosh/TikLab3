﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using Microsoft.Win32;

namespace TIKLab
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string _alphabet;
        private MainTreeViewModel _mainTreeView;
        private HoffmanCodeBuilder _hoffmanBuilder;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void MenuItemOpen_OnClick(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
                InputTextBox.Text = File.ReadAllText(openFileDialog.FileName);
            _alphabet = null;
            TabItemText.IsSelected = true;
        }

        private void OpenAlphabetDialog(object sender, RoutedEventArgs e)
        {
            var alphabetEditor = new AlphabetEditor(InputTextBox.Text, _alphabet);
            alphabetEditor.ShowDialog();
            _alphabet = alphabetEditor.GetAlphabet();

            if(!alphabetEditor.DeleteNonAlphabetSymbols) return;

            var text = InputTextBox.Text;
            var allSymbols = new HashSet<char>(text.ToLower().ToCharArray());
            var alphabet = new HashSet<char>(_alphabet.ToCharArray());
            foreach (var symbol in alphabet)
                allSymbols.Remove(symbol);
            var symbolsToRemove = allSymbols.ToArray();
            var cleanString = string.Join("", text.Split(symbolsToRemove));

            InputTextBox.Text = cleanString;
        }

        private void CalculateStats(object sender, RoutedEventArgs e)
        {
            var alphabet = _alphabet?.ToCharArray();
            var textStatAnalyzer = new TextStatAnalyzer(InputTextBox.Text, alphabet);
            textStatAnalyzer.RecalculateStats();
            var occurances = textStatAnalyzer.GetSymbolOccurances();
            _hoffmanBuilder = new HoffmanCodeBuilder(occurances, alphabet);
            _mainTreeView = new MainTreeViewModel(_hoffmanBuilder.RootNode);
            HoffmanTreeView.DataContext = _mainTreeView;
            CodesText.Text = _hoffmanBuilder.GetCodes();

            TabItemTreeAndCodes.IsSelected = true;
        }

        private void MenuItemClear_OnClick(object sender, RoutedEventArgs e)
        {
            InputTextBox.Text = "";
            _alphabet = null;
            TabItemText.IsSelected = true;
        }

        private void MenuItemGenerate_OnClick(object sender, RoutedEventArgs e)
        {
            var textGenerationDialog = new RandomTextDialog();
            if (textGenerationDialog.ShowDialog() == true)
                InputTextBox.Text = textGenerationDialog.Answer;
            TabItemText.IsSelected = true;
        }

        private void ButtonEncode_OnClick(object sender, RoutedEventArgs e)
        {
            if(_hoffmanBuilder == null) return;

            var textOriginal = OriginalText.Text;
            var sizeOriginal = textOriginal.Length*16;
            var textConverted = _hoffmanBuilder.EncodeString(textOriginal);
            var sizeConverted = textConverted.Length;
            EncodedText.Text = textConverted;

            SizeOriginalLabel.Text = $"Размер изначальный = {sizeOriginal}";
            SizeConvertedLabel.Text = $"Размер сжатого текста = {sizeConverted}";
            SizeCompression.Text = $"Процент сжатия = {1-sizeConverted/(float)sizeOriginal}";

        }

        private void ButtonDecode_OnClick(object sender, RoutedEventArgs e)
        {
            DecodingState state;
            OriginalText.Text = _hoffmanBuilder.DecodeString(EncodedText.Text, out state);

            if (state == DecodingState.ErrorIncorrectSymbol)
                MessageBox.Show("В строке обнаружен недопустимый символ", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            else if (state == DecodingState.ErrorCodeIsTooLong)
                MessageBox.Show("Слишком большая длина кодового слова", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
        }

    }
}
