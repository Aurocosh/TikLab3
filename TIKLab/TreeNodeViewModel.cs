﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace TIKLab
{
    public class TreeNodeViewModel : INotifyPropertyChanged
    {
        #region Data

        private readonly TreeNode _node;

        private bool _isExpanded;
        private bool _isSelected;

        #endregion // Data

        #region Constructors

        public TreeNodeViewModel(TreeNode node) : this(node, null)
        {
        }

        private TreeNodeViewModel(TreeNode node, TreeNodeViewModel parent)
        {
            _node = node;
            Parent = parent;

            Children = new ReadOnlyCollection<TreeNodeViewModel>(
            (
                from child in _node.GetNodes() 
                select new TreeNodeViewModel(child, this)
            ).ToList());
        }

        #endregion // Constructors

        #region Node Properties

        public ReadOnlyCollection<TreeNodeViewModel> Children { get; }

        public string Name
        {
            get
            {
                if (_node.IsLeaf)
                {
                    return _node.Symbol + " " + _node.SymbolCount;
                }
                return _node.SymbolCount.ToString();
            }
        }


        #endregion // Node Properties

        #region Presentation Members

        #region IsExpanded

        /// <summary>
        /// Gets/sets whether the TreeViewItem 
        /// associated with this object is expanded.
        /// </summary>
        public bool IsExpanded
        {
            get { return _isExpanded; }
            set
            {
                if (value != _isExpanded)
                {
                    _isExpanded = value;
                    OnPropertyChanged("IsExpanded");
                }

                // Expand all the way up to the root.
                if (_isExpanded && Parent != null)
                    Parent.IsExpanded = true;
            }
        }

        #endregion // IsExpanded

        #region IsSelected

        /// <summary>
        /// Gets/sets whether the TreeViewItem 
        /// associated with this object is selected.
        /// </summary>
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (value != _isSelected)
                {
                    _isSelected = value;
                    OnPropertyChanged("IsSelected");
                }
            }
        }

        #endregion // IsSelected

        #region NameContainsText

        public bool NameContainsText(string text)
        {
            if (string.IsNullOrEmpty(text) || string.IsNullOrEmpty(Name))
                return false;

            return Name.IndexOf(text, StringComparison.InvariantCultureIgnoreCase) > -1;
        }

        #endregion // NameContainsText

        #region Parent

        public TreeNodeViewModel Parent { get; }

        #endregion // Parent

        #endregion // Presentation Members        

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion // INotifyPropertyChanged Members

    }
}
