﻿using System;
using System.Collections;
using System.Data;
using System.Text;

namespace TIKLab
{
    public static class StaticHelper
    {
        public static DataTable ToDataTable<T>(T[,] matrix, string[] columns)
        {
            var columnCount = matrix.GetLength(0);
            var columnNamesCount = columns.Length;
            var rowCount = matrix.GetLength(1);

            var table = new DataTable();

            table.Columns.Add(new DataColumn(""));
            for (var i = 0; i < columnCount; i++)
            {
                var columnName = i < columnNamesCount ? columns[i] : i.ToString();
                table.Columns.Add(new DataColumn(columnName));
            }

            for (var row = 0; row < rowCount; row++)
            {
                var rowName = row < columnNamesCount ? columns[row] : row.ToString();
                var newRow = table.NewRow();
                newRow[0] = rowName;
                for (var column = 0; column < columnCount; column++)
                    newRow[column+1] = matrix[column, row];
                table.Rows.Add(newRow);
            }
            return table;
        }

        public static void RoundAllValuesInArray(ref double[] array, int accuracy)
        {
            int size = array.Length;

            for (int i = 0; i < size; i++)
                array[i] = (float)Math.Round(array[i], accuracy);
        }

        public static void RoundAllValuesInArray(ref double[,] array, int accuracy)
        {
            int columns = array.GetLength(0);
            int rows = array.GetLength(1);

            for (int i = 0; i < columns; i++)
                for (int j = 0; j < rows; j++)
                    array[i, j] = Math.Round(array[i, j], accuracy);
        }

        public static void PrintMatrix(double[,] matrix)
        {
            int colLength = matrix.GetLength(0);
            int rowLength = matrix.GetLength(1);

            for (int i = 0; i < rowLength; i++)
            {
                for (int j = 0; j < colLength; j++)
                {
                    Console.Write($"{matrix[j, i]:##.000}\t");
                }
                Console.Write(Environment.NewLine);
            }
        }

        public static string ToBitString(this BitArray bits)
        {
            var sb = new StringBuilder();

            for (int i = 0; i < bits.Count; i++)
            {
                char c = bits[i] ? '1' : '0';
                sb.Append(c);
            }

            return sb.ToString();
        }
    }
}
