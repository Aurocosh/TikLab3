﻿using System.Collections.Generic;
using System.Linq;

namespace TIKLab
{
    public class TextStatAnalyzer
    {
        public string Text;
        public char[] Alphabet;
        private const double Epsilon = 0.0000001f;
        private int _symbolCount;

        private int[] _symbolOccurances;
        private double[] _symbolProbabilities;

        public TextStatAnalyzer(string text = null, char[] alphabet = null)
        {
            Text = text;
            Alphabet = alphabet;
        }
#region Getters

        public int GetAlphabetCount()
        {
            return Alphabet.Length;
        }

        public int GetSymbolCount()
        {
            return _symbolCount;
        }

        public int[] GetSymbolOccurances()
        {
            return _symbolOccurances;
        }

        public double[] GetSymbolProbabilities()
        {
            return _symbolProbabilities;
        }

#endregion

        public bool RecalculateStats()
        {
            RecalculateAphabet();
            RecalculateOccurances();
            RecalculateProbabilities();
            return ValidateProbabilities();
        }

        private void RecalculateAphabet()
        {
            if (Alphabet != null) return;
            Alphabet = TextToAlphabet(Text);
        }

        private void RecalculateOccurances()
        {
            int i = 0;
            var occurances = Alphabet.ToDictionary(symbol => symbol, symbol => i++);
            _symbolOccurances = new int[Alphabet.Length];
            foreach (var symbol in Text)
            {
                var lowerSymbol = char.ToLower(symbol);
                if (occurances.ContainsKey(lowerSymbol))
                {
                    _symbolOccurances[occurances[lowerSymbol]]++; _symbolCount++;
                }
            }
        }

        private void RecalculateProbabilities()
        {
            _symbolProbabilities = new double[_symbolOccurances.Length];
            int i = 0;
            foreach (var occuranceCount in _symbolOccurances)
                _symbolProbabilities[i++] = (double)occuranceCount / _symbolCount;
        }

        public bool ValidateProbabilities()
        {
            var sum = _symbolProbabilities.Sum();
            var difference = 1 - sum;
            return difference < Epsilon;
        }

        public static char[] TextToAlphabet(string text)
        {
            var charSet = new HashSet<char>();
            var excludedChars = new HashSet<char> { '\r', '\n', '\t' };
            foreach (var symbol in text)
                if (!excludedChars.Contains(symbol))
                    charSet.Add(char.ToLower(symbol));

            var charSetArray = (charSet.OrderBy(c => c)).ToArray();
            return charSetArray;
        }
    }
}
