﻿using System.Windows;
using NLipsum.Core;

namespace TIKLab
{
    /// <summary>
    /// Interaction logic for RandomTextDialog.xaml
    /// </summary>
    public partial class RandomTextDialog : Window
    {
        public string Answer => TextBoxMain.Text;

        public RandomTextDialog()
        {
            InitializeComponent();
        }

        private void ButtonGenerate_OnClick(object sender, RoutedEventArgs e)
        {
            var rawText = Lipsums.LoremIpsum;
            var lipsum = new LipsumGenerator(rawText, false);

            var desiredParagraphCount = IntegerUpDownParagraphs.Value ?? 1;
            var generatedParagraphs = lipsum.GenerateParagraphs(desiredParagraphCount, Paragraph.Medium);
            TextBoxMain.Text = string.Join("\n\n", generatedParagraphs);
        }

        private void ButtonAccept_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
