﻿using System.Collections.Generic;
using Priority_Queue;

namespace TIKLab
{
    public class TreeNode : FastPriorityQueueNode
    {
        private TreeNode _rightNode;
        private TreeNode _leftNode;

        public int SymbolCount { get; }
        public char Symbol { get; }

        public bool IsLeaf => LeftNode == null && RightNode == null;
        public bool GotLeftNode => LeftNode != null;
        public bool GotRightNode => RightNode != null;

        public TreeNode ParentNode { get; set; }
        public TreeNode LeftNode
        {
            get { return _leftNode; }
            set
            {
                _leftNode = value;
                _leftNode.ParentNode = this;
            }
        }
        public TreeNode RightNode
        {
            get { return _rightNode; }
            set
            {
                _rightNode = value;
                _rightNode.ParentNode = this;
            }
        }

        public TreeNode(int symbolCount, char symbol)
        {
            SymbolCount = symbolCount;
            Symbol = symbol;
        }

        public TreeNode(TreeNode leftNode, TreeNode rightNode)
        {
            SymbolCount = leftNode.SymbolCount + rightNode.SymbolCount;
            LeftNode = leftNode;
            RightNode = rightNode;
        }

        public List<TreeNode> GetNodes()
        {
            var nodes = new List<TreeNode>();
            if (LeftNode != null)
                nodes.Add(LeftNode);
            if (RightNode != null)
                nodes.Add(RightNode);
            return nodes;
        }
    }
}
