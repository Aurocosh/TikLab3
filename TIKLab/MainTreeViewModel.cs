﻿using System.Collections.ObjectModel;

namespace TIKLab
{
    public class MainTreeViewModel
    {
        public ReadOnlyCollection<TreeNodeViewModel> FirstGeneration { get; }

        public MainTreeViewModel(TreeNode rootNode)
        {
            var rootNodeView = new TreeNodeViewModel(rootNode);
            FirstGeneration = new ReadOnlyCollection<TreeNodeViewModel>(new[] {rootNodeView});
        }
    }
}
